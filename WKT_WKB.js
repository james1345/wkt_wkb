/* 
 * Copyright (C) James McMahon 2013
 * 
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the X11 Lisence (see COPYING).
 * 
 */
 /*
  * A pure javascript library for converting between the OGC WKT and WKB
  * representations for geometric data types.
  */

var OGC = {
	Geometry:{
		
		Endianness: {
			XDR: 0, // Big endian
			NDR: 1 // Little endian
		},
		
		// Turn a string representing a double in binary into a double
		ParseDouble: function(binaryString, endianness){
			
			if(binaryString.length() != 64){
				throw "Doubles must be 64 bits!";
			}
			
			var sign = 0.0, exponent = 0.0, significand = 0.0;
			var start =0, diff =1, end = 64;
			if(endianness == Endianness.NDR) { //little endian, reverse processing
				throw "NDR not yet implemented";
			}
			
			sign = binaryString[0] == "0" ? 1.0 : -1.0;
			for(var i = 1; i < 8; i++){
				exponent += parseFloat(binaryString[i]);
				exponent = exponent << 1;
			}
			for(var i = 8; i < 64; i++){
				significand += parseFloat(binaryString[i]);
				significand = significand << 1;
			}
			
			
		},
		
		CreatePoint: function(x, y) {
			if(
			return {x, y};
		}
		
		WkbToWkt: function(wkbBinaryString){
			if(something) doSomething();
			return null;
		}
		
		
		
		WktToWkb(wktString){
			
		}
		
		
	}
}
